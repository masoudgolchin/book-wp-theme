<?php get_header(); ?>
    <div class="content">
        <?php
            if(have_posts()) :
                while(have_posts()) : the_post();
        ?>
        <div class="book">
            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <div class="entry">
                <?php the_content(); ?>
            </div>
            <div class="info">
                <?php the_taxonomies( 'before=<ul>&after=</ul>' ); ?>    
            </div>
        </div>
        <?php
                endwhile;
            else:
                echo "No Posts!";
            endif;
        ?>
    </div>

<?php get_footer(); ?>
