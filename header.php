<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title><?php wp_title(); ?></title>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    </head>
<body>
<div class="header">
    <h1><?php echo get_bloginfo('site_title'); ?></h1>
</div>
