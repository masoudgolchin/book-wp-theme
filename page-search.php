<?php
/*
Template Name: Search ISBN
*/
get_header();
?>
    <div class="content">
        <h3>Search ISBN</h3>
        <div class="search">
            <form method="GET">
                <input type="text" name="isbn" required/>
                <button>search</button>
            </form>
        </div>

        <?php echo do_shortcode('[search_isbn]'); ?>
        
    </div>
<?php get_footer(); ?>
